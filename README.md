# py-model-api-server

## Term files

### Refering to individuals with disability
* https://www.ungeneva.org/sites/default/files/2021-01/Disability-Inclusive-Language-Guidelines.pdf
* https://pwd.org.au/resources/disability-info/language-guide/
* https://www.afdo.org.au/news/language-guide/
* https://www.judicialcollege.vic.edu.au/resources/disability-access-bench-book

### Genered language
* **(TODO)** https://www.stylemanual.gov.au/format-writing-and-structure/inclusive-language/gender-and-sexual-diversity
* Gaucher, Danielle, Justin Friesen, and Aaron C. Kay. ‘Evidence That Gendered Wording in Job Advertisements Exists and Sustains Gender Inequality.’ Journal of Personality and Social Psychology 101, no. 1 (July 2011): 109–28. https://doi.org/10.1037/a0022530.
