from typing import Optional
from pydantic import BaseModel

class JobDocumentMeta(BaseModel):
    groupId: str
    webId: str

class JobCategoryInfo(BaseModel):
    title: str
    description: str
    category: Optional[str] = ""
    subcategory: Optional[str] = ""
    company: str


class JobInfo(BaseModel):
    title: str
    description: str
    meta: JobDocumentMeta


class PayInfo(BaseModel):
    payString: str
    description: str
    jobTitle: str


class JobTypeInfo(BaseModel):
    value: str
    collectionName: str


class EmbeddingConf(BaseModel):
    n_window: int = 4
    percentile: int = 95
    percentile_cutoff: int = 0
    detailed: bool = False
    split_by_sentence: bool = False


class EmbeddingInput(BaseModel):
    text: str
    conf: EmbeddingConf


class FullJobInfo(BaseModel):
    title: str
    description: str
    payInfoText: str
    jobTypeText: str
    collectionName: str
    dateListed: int
    company: str
    category: Optional[str]
    subcategory: Optional[str]
    softskill_embed_conf: EmbeddingConf

class PostInfo(BaseModel):
    fullJobInfo: FullJobInfo
    processedData: dict


class DateNumber(BaseModel):
    number: int


class TextOnly(BaseModel):
    text: str


class PhrasePair(BaseModel):
    phrase1: str
    phrase2: str