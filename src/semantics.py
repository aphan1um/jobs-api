from fastapi import FastAPI, APIRouter, Request, Response, Body
from fastapi.routing import APIRoute

from typing import Callable, List

from util.core import getDataFileLocation
from util.word2vec import MiniLMPhraseModel, to_document_scores

from basemodels import *

from collections import OrderedDict
import time
import json
import inspect

def create_logs_f(all_logs):
    def create_new_log_group():
        log_group = []
        all_logs[inspect.stack()[1][0].f_code.co_name] = log_group
        return lambda txt: log_group.append(txt)
    return create_new_log_group

class ContextIncludedRoute(APIRoute):
    def get_route_handler(self) -> Callable:
        original_route_handler = super().get_route_handler()

        async def custom_route_handler(request: Request) -> Response:
            start_time = time.time()

            all_logs = OrderedDict()
            request.state.create_new_log_group = create_logs_f(all_logs)

            response: Response = await original_route_handler(request)
            response.headers["Response-Time"] = str(int((time.time() - start_time) * 1000))

            jsonPrint = {'meta': {}, 'locals': {}}

            request_body = await request.body()

            if request.url.path != '/docs' and request_body:
                jsonPrint['level'] = 'info'
                jsonPrint['message'] = f"HTTP {request.method} {request.url.path}"

                jsonPrint['meta']['req'] = {
                    'body': json.loads(request_body),
                    'headers': dict(request.headers.items()),
                }

                jsonPrint['meta']['res'] = {
                    'body': json.loads(response.body),
                    'headers': dict(response.headers.items()),
                }

                jsonPrint['locals']['all_logs'] = all_logs

                print(json.dumps(jsonPrint))

            return response

        return custom_route_handler

app = FastAPI()
app.router.route_class = ContextIncludedRoute
router = APIRouter(route_class=ContextIncludedRoute)


embeddingModel = MiniLMPhraseModel('sentence-transformers/all-MiniLM-L12-v2') # Word2VecPhraseModel('../../model-lexvec.bin')
softskill_names, vectors_softskills = embeddingModel.load_phrase_json_file(getDataFileLocation('soft-skills-compiled.json'))
ageism_names, vectors_ageism = embeddingModel.load_phrase_json_file(getDataFileLocation('discrimination-ageist-phrases.json'))


@router.post('/similarity', tags=['similarity'])
async def get_phrase_similarity(p: PhrasePair):
    unrecognisedWords = set()
    phrase1Tokenised = embeddingModel.tokenise(p.phrase1, unrecognisedWords)
    phrase2Tokenised = embeddingModel.tokenise(p.phrase2, unrecognisedWords)

    return {
        "score": float(embeddingModel.tokensToVector(phrase1Tokenised).dot(embeddingModel.tokensToVector(phrase2Tokenised))),
        "words-unrecognised": unrecognisedWords,
    }


@router.post('/soft-skills')
async def get_soft_skill_percentile(input: EmbeddingInput, request: Request):
    final_result = to_document_scores(input.text, input.conf.n_window, input.conf.percentile,
                                      embeddingModel, vectors_softskills, request.state.create_new_log_group(), split_by_sentences=input.conf.split_by_sentence)

    if final_result is None:
        return {'softskill_scores': {}}

    if input.conf.detailed:
        return {'softskill_scores': dict(sorted(
            {softskill_names[i]: {k: float(v[i]) for k, v in final_result.items() if k != 'length'} for i in range(final_result['length']) if float(final_result['percentile'][i]) >= input.conf.percentile_cutoff/100}.items(), key=lambda item: -item[1]['percentile']))}

    # print(final_result)

    return {'softskill_scores': dict(sorted(
        {softskill_names[i]: float(final_result['percentile'][i]) for i in range(final_result['length']) if float(final_result['percentile'][i]) >= input.conf.percentile_cutoff/100}.items(), key=lambda item: -item[1]))}


@router.post('/ageism')
async def get_ageism_percentile(input: EmbeddingInput):
    final_result = to_document_scores(input.text, input.conf.n_window, input.conf.percentile,
                                      embeddingModel, vectors_ageism, split_by_sentences=input.conf.split_by_sentence)

    if final_result is None:
        return {'ageism_scores': {}}

    if input.conf.detailed:
        return {'ageism_scores': dict(sorted(
            {ageism_names[i]: {k: float(v[i]) for k, v in final_result.items() if k != 'length'} for i in range(final_result['length']) if float(final_result['percentile'][i]) >= input.conf.percentile_cutoff/100}.items(), key=lambda item: -item[1]['percentile']))}

    return {'ageism_scores': dict(sorted(
        {ageism_names[i]: float(final_result['percentile'][i]) for i in range(final_result['length']) if float(final_result['percentile'][i]) >= input.conf.percentile_cutoff/100}.items(), key=lambda item: -item[1]))}


@app.get("/env", tags=['env'])
def env_info():
    with open(getDataFileLocation('env_test.txt')) as f:
        sample_text = f.read()
    return {'version': 1, 'test_message': sample_text}


app.include_router(router)
