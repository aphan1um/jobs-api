from fastapi import FastAPI, APIRouter, Request, Response, Body
from fastapi.routing import APIRoute
from fastapi.encoders import jsonable_encoder
from typing import Callable, List

from uuid import uuid4


from util.coding import get_anzsco_code
from util.postprocess import postprocess_info
from util.wages import get_job_pay_info
from util.text import getCompanyInText, getMinExperienceRequired, getEducationLevelNeeded, getDiscriminatoryCounts, getAgeInfo
from util.core import getDataFileLocation
from util.text_lite import clean_text

from basemodels import *

import json
import httpx
import datetime
import os
import time
import inspect

from collections import OrderedDict


if 'SEMANTIC_API_SERVICE_URL' in os.environ:
    SEMANTIC_API_SERVICE_URL = os.environ['SEMANTIC_API_SERVICE_URL']
else:
    SEMANTIC_API_SERVICE_URL = 'http://localhost:8001'


with open(getDataFileLocation('work_types.json')) as f:
    json_work_types = json.load(f)


def create_logs_f(all_logs):
    def create_new_log_group():
        log_group = []
        all_logs[inspect.stack()[1][0].f_code.co_name] = log_group
        return lambda txt: log_group.append(txt)
    return create_new_log_group

# Thanks to: https://stackoverflow.com/a/64117827
class ContextIncludedRoute(APIRoute):
    def get_route_handler(self) -> Callable:
        original_route_handler = super().get_route_handler()

        async def custom_route_handler(request: Request) -> Response:
            start_time = time.time()

            all_logs = OrderedDict()
            request.state.create_new_log_group = create_logs_f(all_logs)

            response: Response = await original_route_handler(request)

            response.headers["X-UUID"] = str(uuid4())
            response.headers["Response-Time"] = str(int((time.time() - start_time) * 1000))

            jsonPrint = {'meta': {}, 'locals': {}}

            request_body = await request.body()

            if request.url.path != '/docs' and request_body:
                jsonPrint['level'] = 'info'
                jsonPrint['message'] = f"HTTP {request.method} {request.url.path}"

                jsonPrint['meta']['req'] = {
                    'body': json.loads(request_body),
                    'headers': dict(request.headers.items()),
                }

                jsonPrint['meta']['res'] = {
                    'body': json.loads(response.body),
                    'headers': dict(response.headers.items()),
                }

                jsonPrint['locals']['all_logs'] = all_logs

                print(json.dumps(jsonPrint))

            return response

        return custom_route_handler


app = FastAPI()
app.router.route_class = ContextIncludedRoute
router = APIRouter(route_class=ContextIncludedRoute)
client = httpx.AsyncClient()


@router.post('/job')
async def get_info(input: FullJobInfo, request: Request):
    ret_dict = {}

    all_items = [
        await get_anzsco_code_endpoint(JobCategoryInfo(title=input.title, description=input.description, category=input.category, subcategory=input.subcategory, company=input.company), request),
        await get_wage_info_endpoint(PayInfo(payString=input.payInfoText, description=input.description, jobTitle=input.title), request),
        await get_job_type_endpoint(JobTypeInfo(value=input.jobTypeText, collectionName=input.collectionName), request),
        await get_experience_needed(JobInfo(title=input.title, description=input.description), request),
        await get_soft_skill_percentile(EmbeddingInput(text=input.description, conf=input.softskill_embed_conf), request),
        await get_number_of_used_discriminatory_phrases(JobInfo(title=input.title, description=input.description), request),
        await get_date_formatted(DateNumber(number=input.dateListed), request),
        await get_age_info(JobInfo(title=input.title, description=input.description), request),
    ]

    for i in all_items:
        ret_dict.update(i)

    postprocess_info(jsonable_encoder(input), ret_dict)

    return ret_dict

@router.post('/postprocess')
async def postprocess(input: PostInfo):
    postprocess_info(jsonable_encoder(input.fullJobInfo), input.processedData)


@router.post('/job/anzsco')
async def get_anzsco_code_endpoint(info: JobCategoryInfo, request: Request):
    return {'anzsco_code': get_anzsco_code(jsonable_encoder(info), request.state.create_new_log_group())}


@router.post('/job/pay')
async def get_wage_info_endpoint(input: PayInfo, request: Request):
    return jsonable_encoder(get_job_pay_info(
        input.payString, clean_text(input.description), input.jobTitle, request.state.create_new_log_group()))


@router.post('/job/experience')
async def get_experience_needed(input: JobInfo, request: Request):
    input.description = clean_text(input.description)

    return {
        'experience_years': getMinExperienceRequired(input.description, input.meta),
        'education_level': getEducationLevelNeeded(input.description, input.meta),
    }


@router.post('/job/type')
async def get_job_type_endpoint(input: JobTypeInfo, request: Request):
    for k, v in json_work_types[input.collectionName].items():
        if k.lower() in input.value.lower():
            return {'work_type': v}
    return {'work_type': 'unknown'}


@router.post('/discriminatory-counts')
async def get_number_of_used_discriminatory_phrases(input: JobInfo, request: Request):
    return {'phrase_counts': getDiscriminatoryCounts(clean_text(input.description), input.meta)}


@router.post('/age')
async def get_age_info(input: JobInfo, request: Request):
    return {'age_info': getAgeInfo(clean_text(input.description), input.meta, request.state.create_new_log_group())}


@router.post('/date')
async def get_date_formatted(input: DateNumber, request: Request):
    # assumes time is in milliseconds
    return {'date_listed': datetime.datetime.fromtimestamp(input.number).strftime('%Y-%m-%d')}


@router.post('/other/company')
def get_company_from_description(input: TextOnly, request: Request):
    return {'company_name': getCompanyInText(input.text, request.state.create_new_log_group())}


@router.post('/semantics/soft-skills')
async def get_soft_skill_percentile(input: EmbeddingInput, request: Request):
    return (await client.post(f'{SEMANTIC_API_SERVICE_URL}/soft-skills', json=jsonable_encoder(input), timeout=150.0)).json()


@app.get("/env", tags=['env'])
def env_info():
    with open(getDataFileLocation('env_test.txt')) as f:
        sample_text = f.read()
    return {'version': 1, 'test_message': sample_text}


app.include_router(router)

print('Server successfully initialised!')
