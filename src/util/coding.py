import re
import json

from util.core import getDataFileLocation


with open(getDataFileLocation('coding_anzsco.json')) as f:
  coding_anzsco = json.load(f)

def get_anzsco_code(job_json, add_to_logs):
  code = _get_anzsco_code(job_json, coding_anzsco, add_to_logs)
  if code:
    return code
  return 99888 # anzsco code for "Response inadequately described"


def _get_anzsco_code(job_json, current_coding, add_to_logs):
  final_value = None

  for entry in current_coding:
    satisfied = False
    idx = 0
    idx_satisfy = len([e for e in entry.keys() if not (e.startswith('_') or e.startswith('?'))])

    for k, v in entry.items():
      orOperationPerformed = False
      listSatisfied = True

      if k.startswith('_'):
        continue

      if type(v) is list:
        re_checks = v
      else:
        re_checks = [v]

      if k.startswith('?'):
        orOperationPerformed = True

      for reExpression in re_checks:
        try:
          if k.startswith('#'):
            searchResult = re.search(reExpression, job_json[k[1:]])
            if searchResult is None:
              listSatisfied = False
              break
          elif k.startswith('?'):
            searchResult = re.search(reExpression, job_json[k[1:]], re.IGNORECASE)
            if searchResult is None:
              listSatisfied = False
              break
          elif k.startswith('!'):
            searchResult = re.search(reExpression, job_json[k[1:]], re.IGNORECASE)
            if searchResult is not None:
              listSatisfied = False
              break
          else:
            searchResult = re.search(reExpression, job_json[k], re.IGNORECASE)
            if searchResult is None:
              listSatisfied = False
              break
        except Exception as e:
          print("Error with regex: %s %s" % (k, v))
          add_to_logs("Error with regex: %s %s" % (k, v))
          raise e
      
      if orOperationPerformed and listSatisfied:
        satisfied = True
        break
      elif orOperationPerformed:
        continue
      elif not listSatisfied:
        break

      idx += 1
      if (idx_satisfy == idx):
        satisfied = True
        break


    if satisfied:
      if '_value' in entry:
        final_value = entry['_value']

      if '_child' in entry:
        child_code = _get_anzsco_code(job_json, entry['_child'], add_to_logs)
        if child_code != None:
          final_value = child_code

    # stop early if anzsco value already found
    if final_value != None:
      break

  return final_value