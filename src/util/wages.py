from typing import List
import json
import re

from util.core import SalaryInfo, getDataFileLocation
from .text import getSalaryStrings, SalaryTextMatch


with open(getDataFileLocation('pay_types.json')) as f:
    pay_types_json = json.load(f)

pay_rate_regexes = []
with open(getDataFileLocation('pay_rate_regex.txt'), 'r', encoding='utf-8') as f:
    for line in f.read().splitlines():
        compiledLine = re.compile(line, re.IGNORECASE)
        pay_rate_regexes.append(compiledLine)


def get_job_pay_info(payString: str, description: str, jobTitle: str, add_to_logs) -> SalaryInfo:
    parsedSalaryInfo: SalaryInfo = parseWageFromString([SalaryTextMatch(raw=payString, match=payString)], add_to_logs)
    parsedTitleInfo: SalaryInfo = parseWageFromString(
        getSalaryStrings(jobTitle, add_to_logs), add_to_logs)

    # look into title to see if there are wage details (if not in payString)
    if parsedSalaryInfo.low is None and not parsedTitleInfo.low is None:
        parsedSalaryInfo.low = parsedTitleInfo.low
        parsedSalaryInfo.high = parsedTitleInfo.high

        # partially avoid phrases in title like "(work only 3 days week!)"
        if parsedSalaryInfo.rate is None:
            parsedSalaryInfo.rate = parsedTitleInfo.rate

    # look into job description if still empty
    if parsedSalaryInfo.low is None or parsedSalaryInfo.rate is None:
        matchStr : List[SalaryTextMatch] = getSalaryStrings(description, add_to_logs)
        add_to_logs(list(map(lambda el: el.dict(), matchStr)))

        if len(matchStr) > 0:
            parsedSalaryDescription = parseWageFromString(matchStr, add_to_logs)

            if parsedSalaryInfo.low is None:
                parsedSalaryInfo.low = parsedSalaryDescription.low
                parsedSalaryInfo.high = parsedSalaryDescription.high

            if parsedSalaryInfo.rate is None:
                parsedSalaryInfo.rate = parsedSalaryDescription.rate

    parsedSalaryInfo = postprocess(parsedSalaryInfo)

    return parsedSalaryInfo


# we make assumptions here
def postprocess(info: SalaryInfo) -> SalaryInfo:
    if info.rate is None and info.low is not None:
        # around over min wage * 52 (https://www.fairwork.gov.au/pay/minimum-wages)
        if info.low >= 40000:
            info.rate = 'annual'
        elif info.high < 70:
            info.rate = 'hour'
        elif info.high < 2000:
            info.rate = 'daily'

    if info.low is not None:
        # example text: $70 - $100k (looks like writer forgot to add 'k' to $70)
        if info.low < 100 and info.high > 45000 and info.low * 1000 <= info.high:
            info.low = info.low * 1000

        # e.g. $110-120,000
        if info.rate == 'annual' and info.high - info.low > 100000 and info.low * 1000 <= info.high:
            info.low *= 1000
        # $800 annual, what
        elif info.rate == 'annual' and info.low < 800:
            info.rate = 'daily'

    # must be for something like salary packaging...
    if info.high is not None and info.high <= 20000 and info.rate == 'annual':
        # info.rate = None
        info.low = None
        info.high = None
    # too high (probably)!
    elif info.low and info.low >= 500000:
        info.rate = None

    return info


def parseWageFromString(payStrings: List[SalaryTextMatch], add_to_logs) -> SalaryInfo:
    def filterMatch(x): 
        return x['obj'].high and not x['obj'].high < 10

    def sortMatch(x):
        score = 0

        if 'salary' in x['text_data'].raw.lower():
            score += 1
        return score


    validMatches = list(filter(
        filterMatch,
        map(lambda x: {'obj': _parseWageFromString(x.match, add_to_logs), 'text_data': x}, payStrings)
    ))

    if len(validMatches) == 0:
        return SalaryInfo()

    validMatches.sort(key=sortMatch, reverse=True)
    return validMatches[0]['obj']


def _parseWageFromString(payString: str, add_to_logs) -> SalaryInfo:
    ret = SalaryInfo()
    lowPay = ''
    highPay = ''
    rateType = ''

    if payString is None:
        return ret

    for k, v in pay_types_json.items():
        if k.lower() in payString.lower():
            ret.rate = v
            break

    for regex in pay_rate_regexes:
        matchResult = regex.search(payString)
        add_to_logs("MATCH %s" % matchResult)
        if matchResult != None:
            break

    validMatch = (matchResult != None)

    # get regex match groups
    if validMatch:
        matchDict = matchResult.groupdict()

        if matchDict['lowPay']:
            lowPay = matchDict['lowPay']
        if matchDict.get('highPay', None):
            highPay = matchDict['highPay']
        if matchDict.get('type', None):
            rateTypeCheck = matchDict['type'].lower().replace(' ', '')
            if rateTypeCheck == '%' or rateTypeCheck == 'year' or rateTypeCheck == 'month':
                validMatch = False

    # parse salary ranges
    if validMatch:
        # assume base salary if high capture group empty
        if lowPay != '' and highPay == '':
            highPay = lowPay

        ret.low, ret.high = parseWagesToInt(lowPay, highPay, rateType, add_to_logs)

        # perform further processing
        if 'up to' in payString and ret.high == 0:  # technicality "up to"
            ret.high = ret.low
            ret.low = 0

        elif ret.rate == 'PayHourly' and ret.low >= 20000:
            ret.jobType = 'PayAnnually'

        elif ret.rate == '' and ret.low <= 50:
            ret.rate = 'PayHourly'

    return ret


def parseWagesToInt(low: str, high: str, rate: str, add_to_logs):
    def preprocess(s: str):
        # return 0 string if not specified
        return s.lower().replace(' ', '').replace(',', '') if s != '' else str(0)

    def postprocess(low: float, high: float, rate: str):
        retLow = low
        retHigh = high

        # arbitrary, yet common sense for a yearly job ($200 net a year? really?)
        if rate == 'PayAnnually' and retLow <= 200 and retHigh <= 200 and retLow * 100 <= retHigh * 100:
            retLow = low
            retHigh = high

        return retLow, retHigh

    # variables
    lowMult = 1
    highMult = 1

    low = preprocess(low)
    high = preprocess(high)

    # process
    if low.endswith('k'):  # example $40K; $20K-$40k but not $20K-40 (which looks strange)
        lowMult *= 1000
        low = low[:-len('k')]

        if high.endswith('k'):
            highMult *= 1000
            high = high[:-len('k')]

    elif high.endswith('k'):
        lowMult *= 1000
        highMult *= 1000
        high = high[:-len('k')]

    lowNum = float(low) * lowMult
    highNum = float(high) * highMult

    # revert if multipler makes low salary range too high
    # TODO: weird conditional
    if highNum < lowNum and (low.endswith('k') or high.endswith('k')):
        if lowNum > 1 and highMult == 1:
            highMult = 1000
            highNum *= highMult

        if highNum < lowNum:
            lowNum /= lowMult

    lowNum, highNum = postprocess(lowNum, highNum, rate)

    # # TODO: EXPERIMENTAL - keep dividing by 10 until we get it within logical bounds
    # if len(high) > 0:
    #     while lowNum > highNum:
    #         lowNum /= 10

    # TODO: Final check to ensure low < high
    if lowNum > highNum:
        highNum = lowNum

    add_to_logs('parseWagesToInt: %s %s' % (lowNum, highNum))
    return int(lowNum), int(highNum)
