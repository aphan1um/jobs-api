
import json
import re

from util.core import getDataFileLocation


with open(getDataFileLocation('post-gig-work.json')) as f:
    gig_work_checks = json.load(f)

def postprocess_info(input_info, final_dict):
    # check if its gig work
    for criteria in gig_work_checks:
      isGigWork = True

      for k, reExpression in criteria.items():
        if k[0] == "<":
          value = input_info[k[1:]]
        elif k[0] == ">":
          value = final_dict[k[1:]]
        else:
          raise Exception("Invalid prefix for post gig work json file")

        if re.search(reExpression, value, re.IGNORECASE) is None:
          isGigWork = False
          break
      
      if isGigWork:
        final_dict["work_type"] = "gig-work"
        break

    # if company isn't specified, try to find in text
    # if input_info["company"] == "":
    #   final_dict["company"] = getCompanyInText(input_info["description"])
    # else:
    #   final_dict["company"] = input_info["description"]
