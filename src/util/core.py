from pydantic import BaseModel
from typing import Optional

import numpy as np
import json

from os.path import abspath


import os

if 'DATA_DIR_RELATIVE_LOC' in os.environ:
    RELATIVE_FILE_LOC = os.environ['DATA_DIR_RELATIVE_LOC']
else:
    RELATIVE_FILE_LOC = '../../core-data/jobs-api'

def getDataFileLocation(fileName):
    print("Getting data file with filename: %s" % (fileName))
    return abspath(RELATIVE_FILE_LOC + '/' + fileName)


class SalaryInfo(BaseModel):
    low: Optional[int]
    high: Optional[int]
    rate: Optional[str]


class SalaryTextMatch(BaseModel):
    raw: str
    match: str


class PhraseModel:
    def load_phrase_json_file(self, path):
        with open(path) as f:
            category_vectors = []
            categories = []

            for category, phrases in json.load(f).items():
                phrase_vectors = []
                for p in phrases:
                    p_tokenised = self.tokenise(p)

                    if len(p_tokenised) > 0:  # ensure the preprocessing didnt make phrase empty string
                        phrase_vectors.append(self.tokensToVector(p_tokenised))

                if len(phrase_vectors) > 0:
                    categories.append(category)
                    # average all phrases in category as a vector
                    category_vectors.append(
                        np.array(phrase_vectors).mean(axis=0))

            ret_vectors = np.array(category_vectors)
            ret_vectors /= np.linalg.norm(ret_vectors, axis=1).reshape(-1, 1)

            return categories, ret_vectors

    def tokenise(self, text):
        pass

    def tokensToVector(self, tokens):
        pass
