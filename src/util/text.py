from typing import List
from numpy import ceil
import spacy
from spacy.matcher import Matcher, DependencyMatcher
from spacy.tokens import Token
from numerizer import numerize
import json
import re
import os
from collections import defaultdict
from collections import Counter

from nltk.corpus import wordnet as wn
from basemodels import JobDocumentMeta
from util.core import SalaryTextMatch, getDataFileLocation

from .text_lite import splitTextIntoSentences

from cachetools import cached, LRUCache
from cachetools.keys import hashkey

# import calendar
# months = list(map(lambda month: month.lower(), list(filter(lambda month: len(
#     month) > 0, list(calendar.month_abbr) + list(calendar.month_name)))))

if 'SPACY_MODEL_NAME' in os.environ:
    nlp = spacy.load(os.environ['SPACY_MODEL_NAME'])
else:
    nlp = spacy.load('en_core_web_trf')

matcher = DependencyMatcher(nlp.vocab)

def spacy_is_noun_person(token):
    try:
        # TODO: rough fix
        for i in range(10):
            is_person = wn.synset('person.n.01') in wn.synset('%s.n.0%d' % (token.text, i)).lowest_common_hypernyms(wn.synset('person.n.01'))
            if is_person:
                return True
    except:
        pass
    return False

def spacy_precedes_person_with(token):
    if token.offset < 2:
        return False

    return token.doc[token.offset - 2].lower_ == "person" and token.doc[token.offset - 1].lower_ == "with"


def cachedKey(*args, **kwargs):
    return (args[0], args[1])

@cached(cache=LRUCache(maxsize=60), key=cachedKey)
def getNLPDocument(text: str, group_id: str, web_id: str):
    return nlp(text)

# [SPACY] add attribute extension to check if noun refers to a person (and not some abstract concept, for example)
# https://stackoverflow.com/questions/55622251/corenlp-can-it-tell-whether-a-noun-refers-to-a-person
Token.set_extension("is_noun_person", getter=spacy_is_noun_person)
Token.set_extension("token_precedes_with_person", getter=spacy_precedes_person_with)

# hard to capture all expressions, so keep a fairly large cap on the expected years needed
MAX_YEARS_EXPERIENCE = 20

with open(getDataFileLocation('spacy-experience-dep.json')) as f:
    for i, pattern in enumerate(json.load(f)):
        matcher.add(str(i), [pattern])

with open(getDataFileLocation('spacy-discrimination-matcher.json'), 'r', encoding='utf-8') as f:
    discrim_matcher = Matcher(nlp.vocab)
    for dcategory, drules in json.load(f).items():
        discrim_matcher.add(dcategory, drules)

with open(getDataFileLocation('spacy-education-matcher.json'), 'r', encoding='utf-8') as f:
    education_matcher = Matcher(nlp.vocab)
    for edulevel, edurules in json.load(f).items():
        education_matcher.add(edulevel, edurules)

with open(getDataFileLocation('spacy-salary-description-matcher.json'), 'r', encoding='utf-8') as f:
    salary_matcher = Matcher(nlp.vocab)
    salary_matcher.add("salary_match", json.load(f), greedy="LONGEST")

with open(getDataFileLocation('spacy-age-matcher.json'), 'r', encoding='utf-8') as f:
    age_matcher = Matcher(nlp.vocab)
    for category, category_rules in json.load(f).items():
        age_matcher.add(category, category_rules, greedy="LONGEST")


with open(getDataFileLocation('company-detection-blacklist.json'), 'r', encoding='utf-8') as f:
    company_detect_blacklist = json.load(f)

EDUCATION_SORT_ORDER = ['highschool', 'certificate', 'university']

def splitTextIntoParagraphs(text: str):
    text = text.strip()
    # note: doing line by line sometimes helps spacy find the experience number better
    paragraphs = re.sub(r"\n+", "\n", text).split("\n")

    return list(map(lambda p: p.strip().removeprefix("*").strip(), paragraphs))

def getCompanyInText(text: str, add_to_logs):
    doc = nlp(text)
    c = Counter([ent.text.lower().removeprefix("the ") for ent in doc.ents if ent.label_ == 'ORG' and not (len(ent.text) <= 4 and ent.text.isupper())])
    common_cs = c.most_common()
    
    if len(common_cs) > 0 and common_cs not in company_detect_blacklist:
        return common_cs[0][0]
    else:
        add_to_logs("Company name ignored or blacklisted: %s" % (text))

    # TODO: Are we still representing empty companies this way
    return ''

def getMinExperienceRequired(description: str, meta: JobDocumentMeta):
    # paragraphs = splitTextIntoParagraphs(description)

    # for para in paragraphs:
    time_duration = _getMinExperienceRequired(description, meta)
    if time_duration:
        return time_duration

    return None


def _getMinExperienceRequired(description: str, meta: JobDocumentMeta):
    # doc = nlp(description)
    doc = getNLPDocument(description, meta.groupId, meta.webId)
    matches = matcher(doc)
    ret = []

    for p in matches:
        match_id, token_ids = p
        _, used_rule = matcher.get(match_id)
        used_rule = used_rule[0]
        new_dict_match = {}
        for i in range(len(token_ids)):
            new_dict_match[used_rule[i]["RIGHT_ID"]
                           ] = numerize(doc[token_ids[i]].text.replace('~', '').replace(',', ''))

        # example: 2/3 years experience as a lead educator or in an equivalent role
        if '/' in new_dict_match['number']:
            new_dict_match['number'] = new_dict_match['number'].split('/')[0]

        if float(new_dict_match['number']) <= MAX_YEARS_EXPERIENCE:
            ret.append(new_dict_match)

    if len(ret) == 0:
        return None

    # TODO: assumes integer for years of experience
    time_duration_type = ret[0]['anchor_time']
    time_duration = float(ret[0]['number'])
    for r in ret:
        if (time_duration_type == 'month') or (time_duration < float(r['number'])):
            time_duration_type = r['anchor_time']
            time_duration = float(r['number'])

    if time_duration_type.startswith('month'):
        time_duration /= 12

    return time_duration


def getEducationLevelNeeded(description: str, meta: JobDocumentMeta):
    doc = getNLPDocument(description, meta.groupId, meta.webId)
    matches = education_matcher(doc)
    # get first match

    education_matches = [doc.vocab.strings[m[0]] for m in matches]
    
    # return the lowest education level from EDUCATION_SORT_ORDER
    for edulevel in EDUCATION_SORT_ORDER:
        if edulevel in education_matches:
            return edulevel

    return None


def getAgeInfo(description: str, meta: JobDocumentMeta, add_to_logs):
    doc = getNLPDocument(description, meta.groupId, meta.webId)
    matches = age_matcher(doc)
    ret = {}

    add_to_logs('getAgeInfo: %s' % matches)

    for match in matches:
        _, start, end = match
        age_text = doc[start:end].text
        age_number = ceil(float(numerize(next(filter(lambda t: t.like_num, doc[start:end])).text).replace(',', '')))

        # avoid getting experience years or unusual age numbers
        if 'experience' in age_text.lower() or age_number >= 100 or age_number <= 10:
            add_to_logs('getAgeInfo match skipped: %s' % age_text)
            continue

        ret[doc.vocab.strings[match[0]]] = age_number
        add_to_logs('getAgeInfo match: %s %s' % (age_text, ret[doc.vocab.strings[match[0]]]))

    return ret


def getDiscriminatoryCounts(description: str, meta: JobDocumentMeta):
    doc = getNLPDocument(description, meta.groupId, meta.webId)
    ret = defaultdict(int)

    for m in discrim_matcher(doc):
        ret[doc.vocab.strings[m[0]]] += 1

    return ret


def getSalaryStrings(description: str, add_to_logs) -> List[SalaryTextMatch]:
    # remove space between commas, "$48- => $48 - "
    description = description.replace(", ", ",")
    description = re.sub(r'(\d)- *(\$?\d)', r'\1 - \2', description)
    # au$42000 -> $42000
    description = re.sub(r"au\$", "$", description.lower())
    # description:$43200 -> description: $43200
    description = re.sub(r"([a-z\$]):([a-z\$])", r"\1: \2", description)

    ret = []

    # TODO: Only look at digits? Probably reasonable if we're talking about stating salaries/wages
    sentences = list(filter(lambda s: any(char.isdigit() for char in s), splitTextIntoSentences(description, nlp)))
    add_to_logs('getSalaryStrings split: %s' % sentences)

    for doc in nlp.pipe(sentences):
        matches = salary_matcher(doc)
        if len(matches) > 0:
            add_to_logs('getSalaryString description: %s' % doc)
            add_to_logs('getSalaryString matches: %s' % matches)
        for match in matches:
            _, start, end = match
            ret.append(SalaryTextMatch(raw=doc.text, match=doc[start:end].text))

    return ret
