import numpy as np
import spacy
import os
from gensim.parsing.preprocessing import preprocess_string, strip_numeric, strip_multiple_whitespaces, remove_stopwords

from gensim.models.keyedvectors import KeyedVectors
from sentence_transformers import SentenceTransformer

from util.core import PhraseModel
from util.text_lite import splitTextIntoSentences


if 'SPACY_MODEL_NAME' in os.environ:
    nlp = spacy.load(os.environ['SPACY_MODEL_NAME'])
else:
    nlp = spacy.load('en_core_web_sm')


class Word2VecPhraseModel(PhraseModel):
    def __init__(self, model_bin_file):
        self.model = KeyedVectors.load(model_bin_file)

    def tokenise(self, text, unrecognised_words: set = None):
        tokenised_list = preprocess_string(text.strip().lower(), filters=[
            strip_multiple_whitespaces, strip_numeric, remove_stopwords])
        return_list = [
            word for word in tokenised_list if word in self.model]

        if unrecognised_words:
            unrecognised_words.update(set(tokenised_list) - set(return_list))

        return return_list

    def tokensToVector(self, tokens):
        return np.array([self.model[word] for word in tokens]).mean(axis=0)


class MiniLMPhraseModel(PhraseModel):
    def __init__(self, model_name):
        self.model = SentenceTransformer(model_name)

    def tokenise(self, text):
        tokenised_list = preprocess_string(text.strip().lower(), filters=[
                                           strip_multiple_whitespaces, strip_numeric])
        return tokenised_list

    def tokensToVector(self, tokens):
        return self.model.encode(' '.join(tokens))


def to_document_scores(text: str, ngram_window: int, percentile: int, embeddingModel: PhraseModel, category_vectors, add_to_logs, split_by_sentences=False):
    textVectors = []

    if split_by_sentences:
        for tokenisedSentence in map(embeddingModel.tokenise, splitTextIntoSentences(text, nlp)):
            print(tokenisedSentence)
            for i in range(len(tokenisedSentence) - ngram_window + 1):
                ngram = tokenisedSentence[i:i+ngram_window]
                textVectors.append(embeddingModel.tokensToVector(ngram))
    else:
        tokenisedText = embeddingModel.tokenise(text)
        for i in range(len(tokenisedText) - ngram_window + 1):
            ngram = tokenisedText[i:i+ngram_window]
            textVectors.append(embeddingModel.tokensToVector(ngram))
    
        # add_to_logs(tokenisedText)


    if len(textVectors) == 0:
        return None

    textVectors = np.array(textVectors)
    # normalise n-gram vectors
    textVectors /= np.linalg.norm(textVectors, axis=1).reshape(-1, 1)

    final_result = category_vectors.dot(textVectors.T)
    ret = {
        'percentile': np.percentile(final_result, percentile, axis=1),
        'mean': np.mean(final_result, axis=1),
        'min': np.min(final_result, axis=1),
        'length': final_result.shape[0]
    }

    # add_to_logs(ret)
    return ret
