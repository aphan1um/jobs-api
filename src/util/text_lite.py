import re

def splitTextIntoSentences(text: str, nlp):
    text = text.strip()
    # note: doing line by line sometimes helps spacy find the experience number better
    paragraphs = re.sub(r"\n+", "\n", text).split("\n")
    sentences = list(map(lambda p: p.strip().removeprefix("*").strip().removesuffix("."), paragraphs))
    return [i.text for i in nlp('. '.join(sentences)).sents]


def clean_text(txt: str):
    # clean description
    txt = txt.replace(chr(160), chr(32))
    txt = txt.replace(chr(8217), chr(39))
    txt = txt.replace(chr(8226), chr(42))
    txt = txt.replace(chr(160), chr(32))
    return txt